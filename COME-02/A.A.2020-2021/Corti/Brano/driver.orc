sr = 44100
ksmps = 5
nchnls = 2
0dbfs = 1 

instr 1 
ifile  = p4
iskip  = p5
idur   = p3
islope = 0.01
iamp   = ampdbfs(-3)

aL,aR mp3in ifile,iskip
kenv linen iamp,islope,idur,islope
	outs aL*kenv,aR*kenv


endin