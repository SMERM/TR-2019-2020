from collezione import Collezione
class Sequenza:
    def __init__(self,file,dur,at=0,metro=96):
        self.collezione = Collezione(file)
        self.dur = dur
        self.at  = at
        self.metro = metro
        self.fragdurs = [1/24.0, 1/24.0, 1/24.0, 1/2.0, 1/16.0, 1/16.0 ]
        self.frag_ind = 0
	self.skip_mod = 3.0
	self.skip_offset = 30.0
	
        
    def run(self):
	endt = self.at + self.dur
	now = self.at
	while (now<endt):
            #frag = self.collezione.prossimo()
            #frag = self.collezione.a_caso() 
            gruppo = self.collezione.prossimo_gruppo()
	    n_repgroup = 10 
 	    for n in range(n_repgroup):
	    	for frag in gruppo:
	   	    dur = self.next_dur()
      	   	    skip = now %self.skip_mod
	   	    skip += self.skip_offset 
           	    print("i1 %8.4f %8.4f %d %8.4f" %(now,dur,frag.num,skip))
           	    now += dur 
                
    def next_dur(self):
        periodo = 60.0/self.metro
        res = 4*periodo*self.fragdurs[self.frag_ind]
        self.frag_ind +=1
        self.frag_ind %= len(self.fragdurs)
        return res
	
	
	
