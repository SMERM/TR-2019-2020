class Frammento:
    def __init__(self,nome,num):
        self.nome = nome 
        self.num  = num

    @classmethod
    def collezione(cls,file):
        res = []
        idx = 1
        with open(file) as line:
            for l in line:
                res.append(Frammento(l.rstrip("\n"),idx))
                idx += 1
        return res
