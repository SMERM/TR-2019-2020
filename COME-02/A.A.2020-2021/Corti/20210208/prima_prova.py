class Sequenza:
	def __init__(self,ritmi,dur,tempo,numfile):
		self.ritmi   = ritmi
		self.dur     = dur
		self.tempo   = tempo
		self.numfile = numfile
		self.curr_ritmo = 0
		
	def generate(self,at,dur): #durata dell'intera sequenza 
		now      = at
		ends     = at+dur
		pulse    = 60.0/self.tempo
		pulseref = 4			#semiminime
		while(now<ends):
			next_at   = now
			next_step = pulseref*self.next_ritmo()*pulse 
			next_dur  = next_step*self.dur
			print("i1 %8.4f %8.4f -8 %d 0 " % (next_at,next_dur,self.numfile))
			now       = now + next_step
	def next_ritmo(self):  
		res = self.ritmi[self.curr_ritmo]
		self.curr_ritmo += 1
		self.curr_ritmo = self.curr_ritmo % len(self.ritmi) 
		return res
		
if __name__ == '__main__':
	s = Sequenza ([1/8.0,1/16.0,1/4.0,1/32.0],0.5,136.0,1)
	s.generate (0,15)
		
	
	
			
			
		