# Esercizi in `pic`

# [`primo.pic`](./primo.pic)

```pic
.PS
scale=2.54
A: box wid 3 ht 8
.PE
```

Questo frammento viene compilato con la linea di comando che segue:

```sh
$ pic primo.pic | groff -P-l -mpic -Tpdf > primo.pdf
```

e produce il file `primo.pdf`:

![primo.pdf](./primo.pdf)
