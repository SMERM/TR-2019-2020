sr      = 48000
ksmps   = 100
nchnls  = 1
0dbfs   = 1






instr 1

kamp linseg 1*p4,p3*0.1,0.8*p4,p3*0.6,0.8*p4,p3*0.1,0.01

aMpoli oscil p6, 100, 2
aMunipol=(aMpoli+1)/2
aC oscil  kamp, p5

                out aC*aMunipol



endin

instr 2

kenvfreq1 linseg 0.2, 60, 0.0002
kampmod linseg 1, 30, 1, 30, 200

  
afm oscil kampmod, 100,2


afreq oscil 200, kenvfreq1, 1

a1 oscil 0.2, 500+afreq+afm, 2
	out a1
endin
