sr      = 48000
kr      = 48000
nchnls  = 1
0dbfs   = 1




instr 1

ioff=0.01

kenv1 expseg 0.01, p3/2, 1, p3/2, 1
kenv2 expseg 1, p3/2, 0.5, p3/2, 0.5

kampcar expseg ioff, p3*0.01, 1*p4, p3-0.01, ioff
kampmod expseg ioff, p3*0.01, 1*p4, p3/2, ioff

aMpoli oscil kampmod-ioff, p6, p7
aMunipol=(aMpoli+kenv1)*kenv2
;display aMunipol, p3
;display aMpoli, p3
a1 oscili kampcar-ioff, p5, p8
aout=a1*aMunipol
display aout, p3
    out aout

endin

instr 3
kenv1 expseg 0.1, p3, 1
kenv2 expseg 1, p3, 2


kamp expseg  1*p4,      p3*0.1,     0.8*p4,     p3*0.6,   0.8*p4,    p3*0.1,     0.01
kamp1 expseg 0.8*p4,    p3*0.1,     0.6*p4,     p3*0.6,   0.4*p4,    p3*0.1,     0.01
kamp2 expseg 0.8*p4,    p3*0.2,     0.5*p4,     p3*0.5,   0.3*p4,    p3*0.1,     0.01
kamp3 expseg 0.6*p4,    p3*0.2,     0.4*p4,     p3*0.5,   0.2*p4,    p3*0.1,     0.01
kamp4 expseg 0.01*p4,   p3*0.4,     0.3*p4,     p3*0.3,   0.2*p4,    p3*0.1,     0.01
kamp5 expseg 0.01*p4,   p3*0.4,     0.2*p4,     p3*0.3,   0.2*p4,    p3*0.1,     0.01

aMpoli oscil 0.5, p6, p7
aMunipol=(aMpoli+kenv1)/kenv2
aCfond oscili  kamp, p5, p8
aCarm1 oscili  kamp1, p5*3, p8
aCarm2 oscili  kamp2, p5*5, p8
aCarm3 oscili  kamp3, p5*7, p8
aCarm4 oscili  kamp4, p5*9, p8
aCarm5 oscili  kamp5, p5*11, p8

aC= aCfond+aCarm1+aCarm2+aCarm3+aCarm4+aCarm5
                out aC*aMunipol



endin

instr 2

kenvfreq1 linseg 0.02, p3/2, 0.5, p3/2, 20
kampmod linseg 1, p3/2, 1, p3/2, 2000
kamp linseg 0, p3/4, 0.05, p3/4, 0.08, p3/4, 0.05, p3/4, 0

afm oscil kampmod, 100,2


afreq oscil 200, kenvfreq1, 1

a1 oscil kamp, 500+afreq+afm, 2
	out a1
endin
