from PappaPronta.voices.voice import Voice
from PappaPronta.events.csound_event import CsoundEvent
import pdb
class ContainerWave(Voice):

    def generate(self):
        instr=self.parameters['instr']
        att=self.parameters['ats']
        durr=self.parameters['duration']
        amposc=self.parameters['amposc']
        ampmod=self.parameters['ampmod']
        freqosc=self.parameters['freqosc']
        freqmod=self.parameters['freqmod']
        end_time = self.at + self.dur
        cur_at = self.at+att.next(self.at)
        cur_dur = 0
        n_note=att.size
        idx=0
        while (idx < n_note):
            #print(cur_at)
            cur_dur = durr.next(cur_at)
            cur_instr = instr.next(cur_at)
            cur_amposc = amposc.next(cur_at)
            cur_ampmod = ampmod.next(cur_at)
            cur_freqosc = freqosc.next(cur_at)
            cur_freqmod= freqmod.next(cur_at)
            freq=self.fields[0]
            cur_freq=freq.next(cur_at)
            amp=self.fields[1]
            cur_amp=amp.next(cur_at)
            #msg = "; this_tempo: %.4f, cur_metro: %.4f, cur_step: %.4f, " % (this_tempo, cur_metro, cur_step)
            self.events.append(CsoundEvent(cur_at, cur_dur, cur_instr, [cur_amposc, cur_ampmod, cur_freqosc, cur_freqmod, cur_freq, cur_amp]))
            cur_at = self.at+att.next(cur_at)
            idx+=1
