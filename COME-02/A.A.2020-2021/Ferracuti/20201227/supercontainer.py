import pdb

from string import Template
import yaml

from PappaPronta.voices.voice import Voice
from PappaPronta.patterns.all_patterns import *
from container import Container

class SuperContainer(Voice):

    def __init__(self, at, dur, cls, pars = {}, flds = [], se = 0):
        self.sub_events = se
        super().__init__(at, dur, cls, pars, flds)

    def generate(self):
        nevs = self.sub_events

        id = 1
        while (id <= nevs):
            at = self.parameters['ats'].next(self.at) 
            this_dict = self.template_reader(id, at)
            this_cls = Container
            msg = "; Container %03d" % (id)
            self.events.append(this_cls.create(this_dict))
            id += 1

    def template_reader(self, id, at):
      template = None
      template_filename = 'container.yml.template'
      with open(template_filename, 'r') as tf:
          template = Template(tf.read())
      values = {
        'this_at' :  self.parameters['ats'].next(at),
        'this_dur' : self.parameters['durs'].next(at),
        # parameters
        'id':    id,
        'instr': self.parameters['instr'].next(at),
        'this_tempo': self.parameters['tempos'][id-1],
        'this_rhythm': self.parameters['rhythms'].next(at),
        'this_duration': self.parameters['durations'].next(at),
        'this_ampmod': self.parameters['ampmods'][id-1],
        'this_frequency': self.parameters['frequencies'][id-1],
        # fields
        'field0': self.fields[0],
        'field1': self.fields[1],
      } 
      yaml_dict = template.substitute(values)
      return yaml.safe_load(yaml_dict)['container']

    #
    # we need to rewrite the create to add the sub-events field
    #
    @classmethod
    def create(cls, dict):
        (at, dur, pars, flds) = cls.parse_dictionary(dict)
        return cls(at, dur, dict['class'], pars, flds, dict['sub_events'])

    #
    # we need to rewrite the check_parameters to be transparent for this class
    #
    def check_parameters(self):
        pass
    #
    # we need to rewrite the parse_dictionary method to be semi-transparent
    #
    @classmethod
    def parse_dictionary(cls, dict):
      at = dict['at']
      dur = dict['dur']
      sub_events = dict['sub_events']
      parameters_to_parse = ['instr', 'ats', 'durs', 'rhythms', 'durations']
      sub_dict = { 'at': at, 'dur': dur, 'parameters': {}, 'fields': []}
      for k in parameters_to_parse:
          sub_dict['parameters'][k] = dict['parameters'][k]
      (at, dur, pars, dummy_fields) = super(SuperContainer,cls).parse_dictionary(sub_dict)
      for k,v in pars.items():
          dict['parameters'][k] = v
      return [at, dur, dict['parameters'], dict['fields']]
