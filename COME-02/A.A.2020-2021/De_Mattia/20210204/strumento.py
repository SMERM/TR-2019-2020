import yaml
import pdb

from componente import Componente

class Strumento:

    def __init__(self, dict):
        self.componenti = []
        self.setup(dict)

    def setup(self, dict):
        ncomp = dict['numero_componenti']
        n = 0
        while (n < ncomp):
            #
            # FIXME: creare componenti con regole prestabilite
            #
            n += 1
        

    def to_csound(self, at, dur, freq):
        output = [c.to_csound(at, dur, freq) for c in self.componenti]
        return ''.join(output)

    @classmethod
    def create(cls, file):
        res = None
        dict = None
        with open(file) as f:
            dict = yaml.load(f)
        names = dict.keys()
        res = [Strumento(dict[n]) for n in names]
        return res
