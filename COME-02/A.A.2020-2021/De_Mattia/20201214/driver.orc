sr= 44100
kr= 44100
0dbfs= 10

gkfr init 0
gkfreq init 0
gkno init 0
instr 1

	kamp linseg 0.0001, p3/10, 1, 4*p3/5, 1, p3/10, 0.0001

	anota oscili p4+0.5+kamp, p4+gkno+80+gkfreq+gkfr, p5

						out anota
endin

instr 2

	gkfr linseg 30, p3/5, 100, p3/5, 300, p3/5, 20, p3/10, 1000, p3/10, 200, p3/5,800

endin

instr 3
	gkno randi 600, 1
	karienv linseg 1, p3, 10
	kenv randi 100, karienv
	kchoice = p4

		if kchoice == 1 then
	gkfreq rspline 50,600, 2, 100
		elseif kchoice == 2 then
	gkfreq randh 20+kenv, 10
		endif

endin
