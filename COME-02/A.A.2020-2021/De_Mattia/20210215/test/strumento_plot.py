import pdb
import yaml
import matplotlib.pyplot as mp

from strumento import Strumento

class StrumentoPlot:

    def __init__(self):
        self.setUp()

    def setUp(self):
        self.conf  = './fixtures/strumento_test.yaml'
        self.dict  = None
        with open(self.conf) as f:
            self.dict = yaml.load(f, Loader=yaml.Loader)
        self.n_strumenti = len(self.dict)


    def plot_exp(self):
        for k,v in self.dict.items():
            str   = Strumento(k, v)
            expy1 = v['deviazione_frequenziale']
            expx1 = v['numero_componenti']
            expy = str.exp(expx1, expy1)
            mp.plot(range(0, expx1), expy)
            mp.xlabel("numero componenti")
            mp.ylabel("deviazione frequenziale")
        mp.savefig("strumento_exp_plot.png")


if __name__ == '__main__':
    sp = StrumentoPlot()
    sp.plot_exp()
