import pdb
import unittest

from strumento import Strumento

class StrumentoTest(unittest.TestCase):

    def setUp(self):
        pdb.set_trace()
        self.conf  = './fixtures/strumento_test.yaml'
        self.strs  = Strumento.create(self.conf)

    def test_creation(self):
        [self.assertTrue(s) for s in self.strs]

if __name__ == '__main__':
    unittest.main()
