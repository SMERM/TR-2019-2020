import yaml
import pdb

import math

from componente import Componente

class Strumento:

    def __init__(self, dict):
        self.componenti = []
        self.setup(dict)

    def setup(self, dict):
        ncomp  = dict['numero_componenti']
        devf   = dict['deviazione_frequenziale']
        deca   = dict['decadimento_ampiezze']
        fi = dict['f_base']
        ai = dict['a_base']
        n = 0
        set_devf = self.exp(ncomp,devf)
        set_deca = self.exp(ncomp,deca)
        while (n < ncomp):
            self.componenti.append(Componente(n,3,ai-set_deca[n],fi*(n+1)*set_devf[n]))
            #
            # FIXME: creare componenti con regole prestabilite
            #
            n += 1


    def to_csound(self, at, dur, freq):
        output = [c.to_csound(at, dur, freq) for c in self.componenti]
        return ''.join(output)

    def linear(self,m):
        pass

    def exp(self,ncomp,m):
        afact = math.log(m)/(ncomp-1)
        a = [math.exp(afact*i) for i in range(0,ncomp-1)]
        return a


    @classmethod
    def create(cls, file):
        res = None
        dict = None
        with open(file) as f:
            dict = yaml.load(f)
        names = [k for k,v in dict.items()]
        res = [Strumento(dict[n]) for n in names]
        return res
