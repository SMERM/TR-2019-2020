class Giulio:
    def __init__(self, n):
        self.number = n

normale = [0, 1, 2, 3]
b = "Io sono b"
a = "Io sono a"
compound = [None, None, [b, 2], [a, 3]]


dict = { 2: [b, 2], 3: [a, 3], 'fava': [a, 4] }

Giulio_compound = [Giulio(x) for x in compound if x]
Giulio_compound_dict = {x: Giulio(dict[x][1]) for x in dict if x}

G_array_from_dict = [Giulio(x[1]) for x in dict.values()]
G_dict_from_array = {x[0]: Giulio(x[1]) for x in compound if x}
