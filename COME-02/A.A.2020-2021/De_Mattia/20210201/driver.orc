sr = 48000
ksmps = 10
nchnls = 1
0dbfs = 1


    instr 1
iamp = ampdbfs(p4)
ifreq = p5
ifn   = p6

kamp expseg 0.001, p3*0.005, iamp, p3*0.1, iamp/3, p3*0.695, iamp/3, p3*0.2, 0.001
kfreq expseg ifreq, p3*0.01, ifreq*1.2, p3*0.01, ifreq*0.8, p3*0.01, ifreq, p3*0.97, ifreq
kviba linseg 0, p3/2, 1, p3/2, 1
kvibf oscil  10*kviba, 6, 1
as  oscili kamp, kfreq+kvibf, ifn

as  linen  as,p3/100,p3,p3/100
    out    as
    
    endin
