fc=48000;
sinc=1/48000;

hwsize = 2**6;
hper = (2*pi)/hwsize;
x = [0:hper:(2*pi)-hper];
hann = (-0.5*cos(x))+0.5;

dur = (2**17)*fc;
tper = dur/hwsize;
t = [-(dur/2):tper:(dur/2)-tper];

figure(1)
stem(t,hann);
print -dpng hann.png

figure(2)

% tcos = [-(dur/2):sinc:(dur/2)-sinc];
% freq = 10;
% hsinc = hper*sinc;
% wx = [0:hsinc:dur-hsinc];
% 
% y = cos(freq*2*pi*tcos);
% 
% yout = y .* wx;
% 
% plot(tcos, yout)

hopsize = hwsize/2;
nwins = 10;
out = zeros(1, ((nwins/2) + 1)*hwsize);
for k=1:nwins
  wstart = k*hopsize;
  wend = wstart + hwsize - 1;
  out(wstart:wend) = out(wstart:wend) .+ hann;
end

plot(out)
print -dpng overlap.png
