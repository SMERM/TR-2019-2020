
class Semifrase():

    cnt = 0

    def __init__(self,dur=None, n_o_mr=None):
        self.dur = dur
        self.n_o_mr= n_o_mr
        self.modi_ritmici=[]
        Semifrase.cnt += 1
        if self.dur == None:
            self.crea_una_semifrase()
        else:
            self.crea_una_semifrase_by_night()


    def crea_una_semifrase(self):
        for i in range(0,2):
            n= Modo_ritmico()
            #print("modi ritmici")
            #print(n)
            self.modi_ritmici.append(n)

    def crea_una_semifrase_by_night(self):
        for i in range(0,self.n_o_mr):
            n= Modo_ritmico(self.dur/2.0)
            self.modi_ritmici.append(n)

    def to_csound(self):
        for g in self.modi_ritmici:
            g.to_csound()

