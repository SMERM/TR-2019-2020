class Suddivisione:
    def __init__(self, l):
        self.unit= l
        self.suddivisione=[]

    def suddividi(self):
        a=self.modo_settiminato()
        b=self.modo_sestinato()
        c=self.modo_quintinato()
        d=self.modo_tetrazzato()
        e=self.modo_terzinato()
        f=self.modo_mezzo()

        self.suddivisione=[a,b,c,d,e,f,self.unit]
        n=self.suddivisione
        #print("ARISUDDIVIDI")
        #print(n)
        #print("###################\n\n")
        return n

    def modo(self, div):
        if self.unit <=0.25:
            return self.unit
        else:
            n = self.unit/div
            return n


    def modo_mezzo(self):
        n = self.unit/2.0
        return n

    def modo_terzinato(self):
        if self.unit <0.25:
            return self.unit
        else:
            n= self.unit/3.0
            return n

    def modo_tetrazzato(self):
        if self.unit <=0.25:
            return self.unit
        else:
            n = self.unit/4.0
            return n

    def modo_quintinato(self):
        if  self.unit<=0.25:
            return self.unit
        else:
            n= self.unit/5.0
            return n

    def modo_sestinato(self):
        if self.unit <=0.25:
            return self.unit
        else:
            n = self.unit/6.0
            return n

    def modo_settiminato(self):
        if self.unit <=0.25:
            return self.unit
        else:
            n=self.unit/7.0
            return n
