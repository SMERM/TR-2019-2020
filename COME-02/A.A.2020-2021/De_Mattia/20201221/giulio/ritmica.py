import random

class Ritmica:
#c'è un problema con crea_la_scansione_durate, def_suddivisione suddividi e Suddivisione
    def __init__(self,dur=None):

        self.dur = dur
        self.scansione_durate=[]
        self.pattern=[]
        if self.dur == None:
            self.crea_la_scansione_durate()

    def scegli_unita_per_la_suddivisione(self):
        l=random.choice(self.scansione_durate)
        return l

    def crea_la_scansione_durate(self):
        self.scansione_durate.append(0.125)
        l= 0.125
        n= int((4)/l)-1
        for i in range (0, n):
            l+=0.125
            self.scansione_durate.append(l)

        #print("SELF.SCANSIONE_DURATE")
        #print(self.scansione_durate)
        #print("###################\n\n")
        return l

#il pattern trasmette le durate delle note

    def suddivisione_by_night(self):
        s = Suddivisione(self.dur)
        array_di_suddivisione = s.suddividi()
        n= self.genera_un_modo_ritmico(array_di_suddivisione)
        return n

    def suddivisione(self):
        l = self.scegli_unita_per_la_suddivisione()
        #print("UNITA DI SUDDIVISIONE SCELTA")
        #print(l)
        #print("###################\n\n")

        s = Suddivisione(l)
        array_di_suddivisione = s.suddividi()
        n= self.genera_un_modo_ritmico(array_di_suddivisione)
        return n


    def genera_un_modo_ritmico(self,array_di_pesca):
        ll= float(array_di_pesca[6])
        #print("questo è ll")
        #print(ll)
        #print("###################\n\n")
        #print("array di pesca")
        #print(array_di_pesca)
        #print("###################\n\n")

        r= array_di_pesca[random.randint(0,6)]
        #print("primo numero dentro al pattern")
        #print(r)
        #print("###################\n\n")
        self.pattern.append(r)
        #print("pattern with r")
        #print(self.pattern)
        #print("###################\n\n")
        u= float(ll)-float(r)
        #print("u")
        #print(u)
        #print("###################\n\n")

        o = random.uniform(0,1)
        #print("o")
        #print(o)
        #print("###################\n\n")

        while  u > 0:
            #print("pattern a ogni giro")
            #print(self.pattern)
            #print("\n\n")

            #print("u")
            #print(u)
            #print("\n\n")

            #print(sum(self.pattern))
            #print("sum\n\n")
            if o > 0.7:
                if (float(self.pattern[0]) == float(array_di_pesca[0])) or (float(self.pattern[0]) == float(array_di_pesca[1])) or (float(self.pattern[0]) == float(array_di_pesca[2])) or (float(self.pattern[0]) == float(array_di_pesca[4])):
                    while u>0:
                        u= u-r
                        if u>=0:
                            self.pattern.append(r)
                            #print(u)

            j=array_di_pesca[random.randint(0,5)]
            #print("j a questo giro")
            #print(j)

            u= float(u)-float(j)
            if (j + sum(self.pattern)) <= ll:
                self.pattern.append(j)
            elif (j + sum(self.pattern)) > ll:
                j=array_di_pesca[random.randint(0,4)]
                if (j + sum(self.pattern)) <= ll:
                    self.pattern.append(j)
                elif (j + sum(self.pattern)) > ll:
                    j=array_di_pesca[random.randint(0,3)]
                    if (j + sum(self.pattern)) <= ll:
                        self.pattern.append(j)
                    elif (j + sum(self.pattern)) > ll:
                        j=array_di_pesca[random.randint(0,2)]
                        if (j + sum(self.pattern)) <= ll:
                            self.pattern.append(j)
                        else:
                            continue
            else:
                self.pattern.append(j)


        #print(self.pattern)
        return self.pattern
