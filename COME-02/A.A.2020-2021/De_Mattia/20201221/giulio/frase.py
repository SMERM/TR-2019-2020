class Frase():

    def __init__(self,dur=None,u_o_s=None):
        self.dur = dur
        self.u_o_s= u_o_s
        self.semifrasi=[]
        if self.dur == None:
            self.crea_una_frase()
        else:
            self.crea_una_frase_by_night()

    def crea_una_frase(self):
        for i in range(0,2):
            n= Semifrase()
            #print("semifrasi")
            #print(n)
            self.semifrasi.append(n)

    def crea_una_frase_by_night(self):
        for i in range(0,3):
            n = Semifrase(self.dur/3.0,2)
            self.semifrasi.append(n)

    def to_csound(self):
        for g in self.semifrasi:
            g.to_csound()

