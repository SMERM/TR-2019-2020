Xp = [0 1 3 4 5 7 8 9];
Yp = [0 1 0.25 0.25 0.6 0.1 0.05 0];

P = polyfit(Xp, Yp, 7);
X = [0:0.01:9];
Y = polyval(P, X);

plot(X, Y);
