import pdb
import unittest
import yaml

from strumento import Strumento

class StrumentoTest(unittest.TestCase):

    def setUp(self):
        self.conf  = './fixtures/strumento_test.yaml'
        self.strs  = Strumento.create(self.conf)
        self.dict  = None
        with open(self.conf) as f:
            self.dict = yaml.load(f, Loader=yaml.Loader)
        self.n_strumenti = len(self.dict)

    def test_creation(self):
        [self.assertTrue(s) for s in self.strs]

    def test_creation_of_multiple_instruments(self):
        self.assertEqual(len(self.strs), self.n_strumenti)

    def test_presence_of_components(self):
        for s in self.strs:
            name = s.name
            dict = self.dict[name]
            ncomp = dict['numero_componenti']
            self.assertEqual(len(s.componenti), ncomp)

    def test_functionality_of_deviazione_frequenziale(self):
        pdb.set_trace()
        for s in self.strs:
            name = s.name
            dict = self.dict[name]
            ncomp = dict['numero_componenti']
            f0 = dict['f_base']
            flast = f0*(ncomp-1)*dict['deviazione_frequenziale']
            self.assertEqual(s.componenti[0].parameters[2], f0)
            self.assertEqual(s.componenti[ncomp-1].parameters[2], flast)

    def test_functionality_of_decadimento_ampiezze(self):
        for s in self.strs:
            name = s.name
            dict = self.dict[name]
            ncomp = dict['numero_componenti']
            a0 = dict['a_base']
            alast = a0*(ncomp-1)*dict['deviazione_frequenziale']
            self.assertEqual(s.componenti[0].parameters[1], a0)
            self.assertEqual(s.componenti[ncomp-1].parameters[1], alast)

if __name__ == '__main__':
    unittest.main()
