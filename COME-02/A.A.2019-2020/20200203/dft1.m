fc = 5000;
sinc = 1/fc;
freq = 500;
dur = 2;
t = [0 : sinc : dur - sinc];
amp = 0.8;
amp2 = 0.2;

y = amp*e.^(i*2*pi*freq*t) + amp2*e.^(i*2*pi*freq*3*t);

nfreq = 1000;
binsize = fc/nfreq;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end

figure(1)
mag = abs(ris)/length(t);
stem(F, mag)

figure(2)
plot(t, real(y), t, imag(y))
axis([0.39 0.41])

figure(3)
plot(t, abs(y))
axis([0.39 0.41])
