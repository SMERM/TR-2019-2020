fc = 5000;
sinc = 1/fc;
freq = 106.2;
dur = 50/freq;
t = [0 : sinc : dur - sinc];
amp = 0.8;

y = amp*cos(2*pi*freq*t);

binsize = 0.1;
nfreq = fc/binsize;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end
figure(1)
mag = abs(ris)/length(t);
stem(F, mag)
axis([-freq*2 freq*2 0 0.45])

figure(2)
stem(t, real(y))
axis([dur-20*sinc dur])
