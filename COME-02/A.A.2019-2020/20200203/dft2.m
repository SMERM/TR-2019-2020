fc = 5000;
sinc = 1/fc;
freq = 106.2;
dur = 0.1;
t = [0 : sinc : dur - sinc];
amp = 0.8;

y = amp*cos(2*pi*freq*t);

nfreq = 1000;
binsize = fc/nfreq;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end
figure(1)
mag = abs(ris)/length(t);
stem(F, mag)
axis([-freq*2 freq*2 0 0.5])

figure(2)
plot(t, y)
axis([0.015 0.045])
