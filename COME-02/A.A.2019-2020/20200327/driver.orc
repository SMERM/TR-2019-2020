
sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1
instr 1
a1 diskin "didjeridoo.wav", p4
out a1
endin

instr 2
kfreq linseg p5, p3*0.2, p5, p3*01, p5*2, p3*0.1, p5/2, p3*0.1, p5, p3*0.2, p5
a1 oscil ampdbfs(p4), kfreq, p6
a1 linen a1, 0.2*p3, p3, 0.5*p3
out a1
endin

instr 3
a1 oscil ampdbfs(p4), p5, p6
a1 linen a1, 0.2*p3, p3, 0.1*p3
out a1
endin

instr 4
kfreq1 linseg p5, p3*0.03, p5/4, p3*0.97, p5/4
a1 oscil ampdbfs(p4), kfreq1, p6
a1 linen a1, 0.05*p3, p3, 0.7*p3
out a1
endin
