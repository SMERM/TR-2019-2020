import random
import re 
class Nota:
    def __init__(self,at,dur,freq,amp,instr):
        self.at=at
        self.dur=dur
        self.freq=freq
        self.amp=amp
        self.instr=instr
        
    def to_csound(self):
        a= "i%d %8.4f %8.4f %8.4f %8.4f \n"%(self.instr,self.at,self.dur,self.freq,self.amp)
        return a 
        
class Generatore:
    def __init__(self,num_note,at,dur,fmin,fmax):
        self.num_note=num_note
        self.at=at
        self.dur=dur
        self.fmin=fmin
        self.fmax=fmax 
        self.note=[]
        self.frequenze=self.genera_frequenze()
        
    def generate(self):
        for x in range(1,self.num_note):
            at =   self.calc_at()
            dur=   self.calc_dur(at)
            freq=  self.calc_freq()
            amp=   self.calc_amp()
            instr= 1 
            n= Nota(at,dur,freq,amp,instr)
            self.note.append(n)
    
    def genera_frequenze(self):
        result=[self.fmin]
        frange=self.fmax-self.fmin
        for n in range(1,4):
            result.append(self.fmin+(frange*(n/4.0)))
        result.append(self.fmax)
        return result
    
    def calc_at(self):                                    
        at=random.uniform(self.at,self.at+self.dur)
        return at
    def calc_dur(self,at):
        dur=random.uniform(at,self.at+self.dur)
        return dur
    def calc_freq(self):
        sz=len(self.frequenze)
        freq=self.frequenze[random.randint(0,sz-1)]
        return freq
    def calc_amp (self):
        amp=-30
        return amp 
    def to_csound(self):
        str="" 
        for x in self.note:
            str+=x.to_csound()
        return str
    
class Composizione :
    def __init__(self, file):
        self.file=file
        self.generatori=[]
        self.create()
    def create (self):
        f=open(self.file,"r")
        lines=f.readlines()
        f.close()
        for j in lines:
            if re.match(r'^\s*#',j):                           #^=inizio riga / *=qualsiasi numero di carattere spazio
                continue
            (nn,at,dur,fmin,fmax)=j.split(" ")
            self.generatori.append(Generatore(int(nn),float(at),float(dur),float(fmin),float(fmax)))
    def to_csound(self):
        print("f1 0 4096 10 1")
        for g in self.generatori:
            g.generate()
            print(g.to_csound())
            
c=Composizione("file.meta") 
c.to_csound() 
