class Nota:
    def __init__(self,at,dur,freq,amp,instr,table):
        self.at=at
        self.dur=dur
        self.amp=amp
        self.freq=freq
        self.instr=instr
        self.table=table

    def to_csound(self):
        a="i%d %8.4f %8.4f %8.4f %8.4f %d\n" %(self.instr,self.at,self.dur,self.freq,self.amp,self.table)
        return a
