# Materiale realizzato da Francesco Ferracuti

## [`pianeti.py`](./pianeti.py)

```python
from nota import Nota
def generate(num_note,number):
    result=[]
    at=0.0
    metro=60/7000
    fstep=2.5
    frequenze=[0,243,605,637,338]
    for n in range(0,num_note-1):
        if number==1:
            step=metro*88
            result.append(Nota(at,step,frequenze[number],-8,1,1))
            at+=step
            frequenze[number]+=fstep
        elif number==2:
            step=metro*225
            result.append(Nota(at,step,frequenze[number],-8,1,2))
            at+=step
            frequenze[number]+=fstep
        elif number==3:
            step=metro*365
            result.append(Nota(at,step,frequenze[number],-8,1,1))
            at+=step
            frequenze[number]-=fstep
        elif number==4:
            step=metro*687
            result.append(Nota(at,step,frequenze[number],-8,1,2))
            at+=step
            frequenze[number]-=fstep
    return result
print("f1 0 4096 10 1\nf2 0 16384 10 1 0   0.3 0    0.2 0     0.14 0     .111 ")
mercurio=generate(227,1)
venere=generate(149,2)
terra=generate(108,3)
marte=generate(57,4)
for n in mercurio:
    print(n.to_csound())
for n in venere:
    print(n.to_csound())
for n in terra:
    print(n.to_csound())
for n in marte:
    print(n.to_csound())
```

Questo *script* produce il risultato che segue:

![pianeti.png](./pianeti.png)
