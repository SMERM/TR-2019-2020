# Lezione di lunedì 13 marzo 2020 (lezione svolta in remoto via zoom)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica (2 ore)

* Strumentario della composizione musicale elettroacustica:
  * tecniche di sintesi
    * additiva

#### Compiti per casa

* frammento con soli suoni sintetici in sintesi additiva
* simulare il *trencadis* di Gaudì
* *phonomosaico*

### Elaborazione Numerica dei Segnali (2 ore)

* Analisi della forma d'onda
  * *Discrete Fourier Transform* (*DFT*)
    * di un segnale reale (*real-world* example)
    * problematiche riguardanti la finestratura del segnale
    * finestratura come modulazione d'ampiezza
    * introduzione alla modulazione d'ampiezza

#### Codice `octave` prodotto

##### [Analisi di un suono reale (didjeridoo.wav)](./didjeridoo.m)

```matlab
[y fc]=audioread("didjeridoo.wav");
y=y';
start=5000;
nsamples=10000;
dur=length(y)/fc;
binsize=fc/nsamples;
f=[-fc/2:binsize:fc/2-binsize];
sinc=1/fc;
t=[0:sinc:dur-sinc];
yfrag = y(start : start+nsamples-1);
tfrag = t(start : start+nsamples-1);
mydft = zeros(1,nsamples);
for k=1:length(f)
	anal = e.^(-i*2*pi*f(k)*tfrag);
	temp = sum(anal.*yfrag);
	mydft(1,k) = temp;
end	
mag = (abs(mydft)/nsamples)*2;

figure(1)
stem(f,mag)
axis([0 2000])
```

Questo script produce il grafico seguente:

[dijeridoo0.jpg](./didjeridoo0.jpeg)

In questa trasformata di Fourier la definizione della trasformazione è
![eq0.png](./eq0.png).


##### [Analisi di un suono reale - collocazione diversa del punto di analisi](./didjeridoo2.m)

```matlab
[y fc]=audioread("didjeridoo.wav");
y=y';
start=20000;
nsamples=10000;
dur=length(y)/fc;
binsize=fc/nsamples;
f=[-fc/2:binsize:fc/2-binsize];
sinc=1/fc;
t=[0:sinc:dur-sinc];
yfrag = y(start : start+nsamples-1);
tfrag = t(start : start+nsamples-1);
mydft = zeros(1,nsamples);
for k=1:length(f)
	anal = e.^(-i*2*pi*f(k)*tfrag);
	temp = sum(anal.*yfrag);
	mydft(1,k) = temp;
end	
mag = (abs(mydft)/nsamples)*2;

figure(1)
stem(f,mag)
axis([0 2000])
```

Questo script produce il grafico seguente:

![didjeridoo2.png](./didjeridoo2.png)

##### [Analisi di un suono reale - finestratura come modulazione d'ampiezza con modulante rettangolare](./didjeridoo3.m)

```matlab
[y fc]=audioread("didjeridoo.wav");
y=y';
start=20000;
nsamples=10000;
dur=length(y)/fc;
sinc=1/fc;
t=[0:sinc:dur-sinc];
modamp=zeros(1, length(y));
modamp(start:start+nsamples-1)=ones(1, nsamples);

result=y.*modamp;
subplot(3,1,1)
plot(t,y)
subplot(3,1,2)
plot(t,modamp)
subplot(3,1,3)
plot(t,result)
```

Questo script produce il grafico seguente:

![didjeridoo3.png](./didjeridoo3.png)

Il primo grafico rappresenta il segnale completo, il secondo la finestra
rettangolare di 10000 campioni, il terzo la moltiplicazione tra i due segnali.

#### [Modulazione d'ampiezza - portante semplice, modulante rettangolare](./modamp0.m)

```matlab
fc= 44100;
sinc=1/fc;
dur=0.2;
t=[0:sinc:dur-sinc];
freqp=100;
port=cos(2*pi*freqp*t);
nsamples=length(t);
modu=[ones(1,nsamples/4) zeros(1,nsamples/2) ones(1,nsamples/4)];
res=port.*modu;
subplot(3,1,1)
plot(t,port)
axis([0 dur -1.2 1.2])
subplot(3,1,2)
plot(t,modu)
axis([0 dur -1.2 1.2])
subplot(3,1,3)
plot(t,res)
axis([0 dur -1.2 1.2])
binsize=fc/length(t);
F=[-fc/2:binsize:fc/2-binsize];
out=zeros(1,length(F));
for k=1:length(F)
	anal=e.^(-i*2*pi*F(k)*t);
	temp=res.*anal;
	out(k)=sum(temp);
end
mag=abs(out)/length(F);
figure(2)
stem(F,mag)
axis([50 150]) 
```

Questo script produce i grafici che seguono:

![segnale modulato - dominio del tempo](./modamp0_1.jpg)

![segnale modulato - dominio della frequenza](./modamp0_1.jpg)

#### [Modulazione d'ampiezza - portante composta, modulante rettangolare](./modamp0.m)

```matlab
fc= 44100;
sinc=1/fc;
dur=0.2;
t=[0:sinc:dur-sinc];
freqp=100;
port=0.8*cos(2*pi*freqp*t)+0.3*cos(2*pi*freqp*2*t);
nsamples=length(t);
modu=[ones(1,nsamples/4) zeros(1,nsamples/2) ones(1,nsamples/4)];
res=port.*modu;
subplot(3,1,1)
plot(t,port)
axis([0 dur -1.3 1.3])
subplot(3,1,2)
plot(t,modu)
axis([0 dur -0.3 1.3])
subplot(3,1,3)
plot(t,res)
axis([0 dur -1.3 1.3])
binsize=fc/length(t);
F=[-fc/2:binsize:fc/2-binsize];
out=zeros(1,length(F));
for k=1:length(F)
	anal=e.^(-i*2*pi*F(k)*t);
	temp=res.*anal;
	out(k)=sum(temp);
end
mag=abs(out)/length(F);
figure(2)
stem(F,mag)
axis([50 300 -0.005 0.21]) 
```

Questo script produce i grafici seguenti:

![modamp1_1.jpg](./modamp1_1.jpg)

![modamp1_2.jpg](./modamp1_2.jpg)

Come si può constatare, le componenti modulanti dell'onda quadra si dispongono
intorno a ciascuna delle componenti della portante.

#### Compiti per casa

* scomposizione di segnali reali armonici e inarmonici
