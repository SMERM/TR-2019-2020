fc= 44100;
sinc=1/fc;
dur=0.2;
t=[0:sinc:dur-sinc];
freqp=100;
port=cos(2*pi*freqp*t);
nsamples=length(t);
modu=[ones(1,nsamples/4) zeros(1,nsamples/2) ones(1,nsamples/4)];
res=port.*modu;
subplot(3,1,1)
plot(t,port)
axis([0 dur -1.2 1.2])
subplot(3,1,2)
plot(t,modu)
axis([0 dur -1.2 1.2])
subplot(3,1,3)
plot(t,res)
axis([0 dur -1.2 1.2])
binsize=fc/length(t);
F=[-fc/2:binsize:fc/2-binsize];
out=zeros(1,length(F));
for k=1:length(F)
	anal=e.^(-i*2*pi*F(k)*t);
	temp=res.*anal;
	out(k)=sum(temp);
end
mag=abs(out)/length(F);
figure(2)
stem(F,mag)
axis([50 150]) 
