sr=44100
ksmps=100
nchnls = 1
0dbfs= 1

instr 3
idur=p3
iamp=p4
ifreq=p5
itable=p6
aout oscil iamp,ifreq,itable
aout linen aout,0.01*idur,idur,0.1*idur
out aout*0.1
endin

