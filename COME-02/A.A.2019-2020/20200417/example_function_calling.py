class Example:

    def instance_method_a(self):
        print "I am method A"


e = Example()

#
# il metodo si puo` chiamare cosi`:
#

e.instance_method_a()

#
# oppure _anche_ cosi`
#
Example.instance_method_a(e)
