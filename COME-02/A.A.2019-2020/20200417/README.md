# Lezione di venerdì 17 aprile 2020 (lezione svolta in remoto via zoom)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle domande

## Argomenti

### Composizione Musicale Elettroacustica

* Strumentario della composizione musicale elettroacustica:
  * utilizzazione di linguaggi generici (in questo caso: `python`) per la
    realizzazione di partiture `csound`:
    * correzione del programma `class0.py`

* Discussione dell'ascolto degli `Études` di Pierre Schaeffer:
  * *Étude aux chemins de fer*
  * *Étude aux casseroles*
  * *Étude aux tourniquets*
  * *Étude de bruits*

## Codice scritto in classe (e a casa)

### [nuova versione di `class0.py`](./class1_rev_1-0.py)

```python
#Compiti prossima volta: modificare orchestra per avere una sonificazione dei dati
#Inviluppo di frequenza con apertura, picco e chiusura
#NON quantizzare le frequenze
#Convertire la data in un AT
#Calcolare la durata per un minutaggio totale di 3 minuti (7 anni -> 3 minuti)

import math

class Nota:
    def __init__ (self, stringa):
        (self.data, self.at, self.dur, self.freq, self.amp, self.table, resto)=stringa.split(',',6)
        self.instr = 3
        self.condizionati()

    def condizionati(self):
        self.at   = float(self.at)
        self.dur  = float(self.dur)
        self.amp  = float(self.amp)
        self.freq = float(self.freq)
        self.table= float(self.table)

    def csound(self):
        print("i%d %8.4f %8.4f %+8.2f %8.4f %d" % (self.instr,self.at, self.dur, self.amp, self.freq, self.table))

class Condizionatore:
    def __init__ (self,nomefile,start=1,stop=-1):
        file=open(nomefile,"r")
        righe=file.readlines()
        file.close()

        self.note=[]
        self.start=start
        self.stop=stop
        for line in righe[self.start:self.stop]:
            self.note.append(Nota(line))

        self.condiziona_il_tutto()

    def manoscritto(self):
        file=open("tsla.sco","w")
        file.write("f1 0 4096 10 1\n")
        for n in self.note[self.start:self.stop]:
            file.write("i%d %8.4f %8.4f %+8.2f %8.4f %d \n" % (n.instr,n.at, n.dur, n.amp, n.freq, n.table))
        file.close()
        print("Score written to file \n")

    def csound(self):
        for n in self.note[self.start:self.stop]:
            n.csound()

    def condiziona_il_tutto(self):
        self.condiziona_gli_at()
        self.condiziona_i_dur()
        self.condiziona_gli_amp()
        self.condiziona_le_freq()
        self.condiziona_le_table()

    def condiziona_gli_at(self):
        min_at=1e10
        for n in self.note[self.start:self.stop]:
            if n.at < min_at:
                min_at=n.at
        for n in self.note[self.start:self.stop]:
            n.at-=min_at

    def condiziona_i_dur(self,min=0.05,max=2):
        real_min = 1e10
        real_max = 0
        for n in self.note[self.start:self.stop]:
            if n.dur < real_min:
                real_min=n.dur                                                  #Finds the maximum
            if n.dur > real_max:
                real_max=n.dur                                                  #Finds the minimum
        norm=max/real_max
        for n in self.note[self.start:self.stop]:
            #print("old dur %8.4f" %(n.dur))                                    #flag
            n.dur=n.dur*norm
            if n.dur==0:
                n.dur=n.dur+min
            #print("new dur %8.4f" %(n.dur) +"\n")                              #flag


    def condiziona_gli_amp(self,min=0.05,max=1):
        real_min = 1e10
        real_max = 0
        for n in self.note[self.start:self.stop]:
            if n.amp < real_min:
                real_min=n.amp                                                  #Finds the maximum
            if n.amp > real_max:
                real_max=n.amp                                                  #Finds the minimum
        norm=(3.14/2)/real_max
        for n in self.note[self.start:self.stop]:
            #print("old amp %8.4f" %(n.amp))                                    #flag
            n.amp=n.amp*norm
            if n.amp==0:
                n.amp=n.amp+min
            #print("inter dur %8.4f" %(n.amp))                                  #flag
            n.amp=math.cos(n.amp)
            #print("new amp %8.4f" %(n.amp) +"\n")                              #flag

    def condiziona_le_freq(self,min=16.35,num_notes=96):
        real_min = 1e10
        real_max = 0
        factor=  2**(1/12)
        for n in self.note[self.start:self.stop]:
            if n.freq < real_min:
                real_min=n.freq                                                 #Finds the maximum
            if n.freq > real_max:
                real_max=n.freq                                                 #Finds the minimum
        norm=num_notes/real_max
        for n in self.note[self.start:self.stop]:
            #print("old freq %8.4f" %(n.freq))                                  #flag
            n.freq=n.freq*norm
            #print("inter freq %8.4f" %(n.freq))                                #flag
            n.freq=min*factor**(n.freq//1)
            #print("new freq %8.4f" %(n.freq) +"\n")                            #flag

    def condiziona_le_table(self,max=1):
        for n in self.note[self.start:self.stop]:
            n.table=1

c = Condizionatore("tsla.txt",1,-1)
c.csound()
c.manoscritto()
```

Questo file produce la partitura

```sh
$ python class1_rev_1-0.py >/dev/null
```

che può poi essere compilata con [l'orchestra che segue](./tsla.orc):

```csound
sr=44100
ksmps=100
nchnls = 1
0dbfs= 1

instr 3
idur=p3
iamp=p4
ifreq=p5
itable=p6
aout oscil iamp,ifreq,itable
aout linen aout,0.01*idur,idur,0.1*idur
out aout*0.1
endin
```

```sh
$ csound --logfile=./tsla.log -dWo ./tsla.wav tsla.orc tsla.sco
time resolution is 1000.000 ns
```

Il prodotto finale è il seguente:

![tsla.wav spettro su scala logaritmica](./tsla_wav_log_spectrum.png)


### [due diversi modi di chiamare i metodi d'istanza in `python`](./example_function_calling.py)

Ci sono due modi diversi per chiamare i metodi d'istanza in `python`. Il primo
utilizza l'istanza come ricevitore, il secondo usa il nome della classe come
ricevitore e l'istanza come argomento. Esempio:

```python
class Example:

    def instance_method_a(self):
        print "I am method A"


e = Example()

#
# il metodo si puo` chiamare cosi`:
#

e.instance_method_a()

#
# oppure _anche_ cosi`
#
Example.instance_method_a(e)
```

```sh
$ python example_function_calling.py
I am method A
I am method A
```

### [risoluzione del problema del passaggio dei puntatori a metodi in `python`](./function_pointer_example.py)

```python
#
# Definisco una classe con un certo numero di metodi diversi
# 
class Slave:

    def __init__(self, name):
        self.name = name

    def i_am(self, f):
        print "I am the function %s of the object %s" % (f, self.name)

    def function_a(self):
        self.i_am('a')

    def function_b(self):
        self.i_am('b')

    def function_c(self):
        self.i_am('c')

#
# questa funzione chiama un metodo di un'istanza di una classe che
# si trova all'interno di una collezione
#

def runner(objs, function):
    for o in objs:
       function(o)

#
# array di istanze della classe Slave
#
ss = [ Slave('s0'), Slave('s1'), Slave('s2') ]

#
# chiamata dei vari metodi della classe slave a turno 
#
runner(ss, Slave.function_c)
runner(ss, Slave.function_b)
runner(ss, Slave.function_a)
```

```sh
$ python function_pointer_example.py
I am the function c of the object s0
I am the function c of the object s1
I am the function c of the object s2
I am the function b of the object s0
I am the function b of the object s1
I am the function b of the object s2
I am the function a of the object s0
I am the function a of the object s1
I am the function a of the object s2
```

Questo esempio risolve il problema incontrato a lezione nel passaggio di
metodi all'interno di funzioni.

## Compiti

### Strumentario

* modificare l'orc in modo da ottenere un inviluppo di frequenza con apertura, picco e chiusura, (non quantizzare le frequenze), convertire la data in un action time,
  calcolare la durata e far durare massimo 3 minuti.
* nuovo file con data: action time; durata:  espressione  da  inventare;
  open high low close : inviluppo di frequenza con possibile  inversione
  tra high e low e trovare un'espressione per sceglisre  inc  che  punto
  temporale inserirli;  volume  rappresenta  sia  ampiezza  che  durata,
  scegliere come farlo.

### Ascolti

* Lavori del WDR Studio di Colonia:
  * Karlheinz Stockhausen:
    * *studie I*
    * *studie II*
* tape music:
  * lavori di Ussachevsky e Luening
