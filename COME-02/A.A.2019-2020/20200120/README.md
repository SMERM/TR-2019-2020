# Lezione di lunedì 20 dicembre 2019

## Argomenti

* Correzione dei compiti
* Risposta alle eventuali domande
* ripasso della formula di Eulero e corollari
* Introduzione all'analisi della forma d'onda
  * scomposizione in serie di Fourier
  * *Discrete Fourier Transform* (*DFT*)
    * realizzazioni in `octave`:
      * segnale complesso, frequenza "intonata" (*bin-synchronous*)

## Lavagne

![whiteboard 1](./TR_I_2020-01-20_18.24.48_1.jpg)

![whiteboard 2](./TR_I_2020-01-20_18.24.48_2.jpg)

## Elaborati `octave`

### [Moltiplicazione tra coseni di frequenze diverse](./cosmul.m)

```matlab
fc = 5000;

sinc = 1/fc ;
X = 10;

x = [0 : sinc : X - sinc ];

w1 = 2 * pi * 10 ; 

w2 = 2 * pi * 500 ;

a1 = 1;
a2 = 0.2;

y1 = cos( w1 * x)*a1;
y2 = cos(w2 * x)*a2;

y = y1 .* y2 ;

plot(x , y1 , x , y2 , x , y , "b" )
axis([1.1 1.2 -1.2 1.2])

yint = sum(y) / length(x)

lm = 1343;

yint2 = sum(y(1:lm))/lm 
```

Questo script `octave` produce l'uscita che segue:

```matlab
yint =    4.2466e-16
yint2 = -0.000021274
```

e il grafico che segue:

![cosmul.jpg](./cosmul.jpg)

### [Quadrato dei coseni (moltiplicazione tra frequenze uguali)](./skucos.m)

```matlab
fc = 5000;

sinc = 1/fc ;
X = 10;

x = [0 : sinc : X - sinc ];

w1 = 2 * pi * 100 ; 

w2 = w1 ;

a1 = 1;
a2 = 0.2;

y1 = cos( w1 * x)*a1;
y2 = cos(w2 * x)*a2;

y = y1 .* y2 ;

plot(x , y1 , x , y2 , x , y , "b" )
axis([1.1 1.2 -1.2 1.2])

yint = sum(y) / length(x)

lm = 1343;

yint2 = sum(y(1:lm))/lm 
```

Questo script `octave` produce l'uscita che segue:

```matlab
yint =  0.100000
yint2 =  0.099755
```

e il grafico che segue:

![skucos.jpg](./skucos.jpg)

### [Moltiplicazione tra esponenziali complessi coniugati tra loro](./skue.m)

```matlab
w1 = 2 * pi * 100 ; 

w2 = w1 ;

a1 = 1;
a2 = 0.2;

y1 = e.^(i * w1 * x)*a1;
y2 = e.^(-i * w2 * x)*a2;

y = y1 .* y2 ;

yint = sum(abs(y)) / length(x)

lm = 1343;

yint2 = sum(abs(y(1:lm)))/lm 
```

Questo script `octave` produce l'uscita che segue:

```matlab
yint =  0.20000
yint2 =  0.20000
```

e il grafico che segue:

![skue.jpg](./skue.jpg)

### [*Discrete Fourier Transform* - versione bin-synchronous (0)](./dft0.m)

```matlab
fc = 5000;
sinc = 1/fc;
freq = 1000;
dur = 2;
t = [0 : sinc : dur - sinc];
amp = 0.8;
y = amp * e.^(i*2*pi*freq*t);
nfreq = 1000;
binsize = fc/nfreq;
F = [-fc/2 : binsize : fc/2 - binsize];
ris = zeros(1, nfreq);
for 	k = 1 : nfreq 
	f= F(k);
	anal = e.^(-i*2*pi*f*t);
	ris(k) = sum(y .* anal);

end
mag = abs(ris)/length(t);
stem(F, mag)
```

Questo script `octave` produce il grafico che segue:

![dft0.jpg](./dft0.jpg)

## Compiti per casa

* verificare che l'algoritmo della *Discrete Fourier Transform* rilevi la
  somma di più cosinusoidi
