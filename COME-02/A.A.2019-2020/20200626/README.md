# Lezione di venerdì 26 giugno 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/0od4HY7xzhU)

## Argomenti

### Composizione Musicale Elettroacustica

* Segmentazione dello spazio temporale:
  * meccanismi funzionamento del ritmo
  * strutture polimetriche
  * strutture poliritmiche
  * strutture polimetronomiche

## Compiti

* realizzazione di un frammento con
  * strutture ritmiche tradizionali (pulsazione, metro, ritmo, ecc.)
  * strutture polimetriche
  * strutture poliritmiche
  * strutture polimetronomiche
