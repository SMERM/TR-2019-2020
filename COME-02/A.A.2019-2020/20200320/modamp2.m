fc= 44100;
sinc=1/fc;
dur=0.2;
t=[0:sinc:dur-sinc];
freqp=100;
port=0.8*cos(2*pi*freqp*t)+0.3*cos(2*pi*freqp*2*t);
nsamples=length(t);
freqm=1/dur;

modu= -0.5*cos(t*2*pi*freqm)+0.5;
res = port.*modu;
subplot(3,1,1)
plot(t,port)
axis([0 dur -1.3 1.3])
subplot(3,1,2)
plot(t,modu)
axis([0 dur -0.3 1.3])
subplot(3,1,3)
plot(t,res)
axis([0 dur -1.3 1.3])
binsize=fc/length(t);
F=[-fc/2:binsize:fc/2-binsize];
out=zeros(1,length(F));
for k=1:length(F)
	anal=e.^(-i*2*pi*F(k)*t);
	temp=res.*anal;
	out(k)=sum(temp);
end
mag=abs(out)/length(F);
figure(2)
stem(F,mag)
axis([50 300 -0.005 0.21])
