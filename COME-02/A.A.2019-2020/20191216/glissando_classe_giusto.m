fc = 2000;			 									%freq. di campionamento
sinc = 1/fc; 											%periodo di camp.
dur = 40;	  											%durata in secondi
t = [0:sinc:dur-sinc];	 								%asse del tempo


hzstart = 0;
hzend = 4000;
amp = 0.9;

%
% Come illustrato qui: https://en.wikipedia.org/wiki/Chirp
% la funzione di un chirp lineare è come segue:
%
% y(t) = cos(2*pi*((c/2)*(t**2) + f0)+phase0)
%
% dove:
%
% * phase0 è la fase iniziale
% * c è il tasso di crescita (lineare) della frequenza, dato da: c = (f1-f0)/dur
%
% il codice che segue viene quindi modificato per implementare questa
% funzione.

c = (hzend-hzstart)/dur;
phase0 = 0;
freqfun = (c/2)*(t.**2) + (hzstart*t);

out1 = cos(2*pi*freqfun+phase0)*amp;

audiowrite("gliss_classe_giusto.wav",out1,fc)
