fc = 48000;			 									%freq. di campionamento
sinc = 1/fc; 											%periodo di camp.
dur = 0.1;  											%durata in secondi
t = [0:sinc:dur-sinc];	 								%asse del tempo


hzstart = 500;
hzend = 3000;

out1 = zeros(length(t)*(hzend-hzstart),1);
y = zeros(length(t),1);
fase = 0;
amp = 0.7;

for k = hzstart:hzend
	globstart = (length(y)*(k-hzstart)) + 1;
	freq = k;
	y = cos(2*pi*t*freq+fase) * amp;
	%fase = (n° di giri) - floor(n°di giri)
	out1(globstart : globstart + length(y)-1) = y ; 
	
end
audiowrite("gliss.wav",out1,fc)

totdur = [0:sinc:dur*(hzend-hzstart+1)-sinc];
