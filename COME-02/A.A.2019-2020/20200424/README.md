# Lezione di venerdì 24 aprile 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/MolkA1uf614)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica

* Strumentario della composizione musicale elettroacustica:
  * utilizzazione di linguaggi generici (in questo caso: `python`) per la
    realizzazione di partiture `csound`:
    * correzione del programma `class0.py`
      (vedi lo schema riportato in [questa figura](./comp_1.png))
    * realizzazione di una seconda composizione, stavolta su dati astratti 

* Discussione dell'ascolto di:

* Karlheinz Stockhausen:
  * *Studie I*
  * *Studie II*
* lavori di tape music di Ussachevsky e Luening

### Lavagne
![whiteboard forme](./comp_1.png)

### Realizzazione in `python` (inizio):

```python
import random
class Nota:
    def __init__(self,at,dur,freq,amp):
        self.at=at
        self.dur=dur
        self.freq=freq
        self.amp=amp

class Triangle:
    def __init__(self,a,b,c,s,e):
        self.a=a
        self.b=b
        self.c=c
        self.start=s
        self.end=e
        self.nota=[]
        self.num_note=random.randint(5,200)

    def generate(self):
        while n<self.num_note:
            at=self.calc_at()
            dur=self.calc_dur()
            freq=self.calc_freq()
            amp=self.calc_amp()
            self.nota.append(Nota(at,dur,freq,amp))



class TriangleLin(Triangle):
    def generate():
        pass

class TriangleExp(Triangle):
    def generate():
        pass

class TriangleCosin(Triangle):
    def generate():
        pass
```

Durante la realizzazione del software, abbiamo anche verificato la creazione
di metodi di istanza e metodi di classe in `python`. Ecco l'esempio:

```python
class A:
    how_many = 0

    def __init__(self, i):
        self.i = i
        A.how_many += 1;

    def show(self):
        print "%d" % (self.i)

    @classmethod
    def how_many_do_we_have(cls):
        print "%d" % (cls.how_many)

#
# istanze
#
a = A(23)
b = A(16)
c = A(42)
d = A(78)

a.show()
b.show()
c.show()
d.show()

#
# metodo di classe
#
A.how_many_do_we_have()
```

## Compiti

### Composizione

* Completare il software per la nuova composizione

### Ascolti

* Ascolto e analisi di Schönberg, *3 pezzi per pianoforte* n.2
