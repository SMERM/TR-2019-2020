from nota import Nota

def generate(num_note,metro):
  step=60.0/metro
  result=[]
  at=0.0
  for n in range(0,num_note-1):
      result.append(Nota(at,step/2,500,-8,1))
      at+=step
  return result

note=generate(30,118)
print("f1 0 4096 10 1")
for n in note:
  print(n.to_csound())
