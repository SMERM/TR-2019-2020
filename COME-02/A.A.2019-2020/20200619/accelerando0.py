from nota import Nota

def tempo_fun(t,start_metro,end_metro,dur_metro):
    a=(end_metro-start_metro)/dur_metro
    b=start_metro
    return a*t+b

def generate(num_note,s_metro,e_metro):
  step=60.0/s_metro
  result=[]
  at=0.0
  for n in range(0,num_note-1):
      result.append(Nota(at,step/2,500,-8,1))
      n_step=60.0/tempo_fun(at,s_metro,e_metro,step*num_note)
      at+=n_step
  return result

note=generate(30,118,300)
print("f1 0 4096 10 1")
for n in note:
  print(n.to_csound())
