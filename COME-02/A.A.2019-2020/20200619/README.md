# Lezione di venerdì 19 giugno 2020 (lezione svolta in remoto via zoom)

## [Video della lezione](https://youtu.be/IZambm0brsE)

## Argomenti

* introduzione al ritmo
* illustrazione della segmentazione di spazi temporali continui
* realizzazione di spazi temporali segmentati con processi continui
  (accelerandi/decelerandi)

## Realizzazioni in classe

### [Pulsazione semplice](./ritmo0.py)

```python
from nota import Nota

def generate(num_note,metro):
  step=60.0/metro
  result=[]
  at=0.0
  for n in range(0,num_note-1):
      result.append(Nota(at,step/2,500,-8,1))
      at+=step
  return result

note=generate(30,118)
print("f1 0 4096 10 1")
for n in note:
  print(n.to_csound())
```

```sh
$ python ritmo0.py > score.sco
$ csound -dWo ./ritmo0.wav driver_1.orc score.sco
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
orchname:  driver_1.orc
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.002s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.002s, CPU: 0.002s
displays suppressed
0dBFS level = 32768.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to ./ritmo0.wav (WAV)
SECTION 1:
ftable 1:
new alloc for instr 1:
B  0.000 ..  0.508 T  0.508 TT  0.508 M:  13043.0
B  0.508 ..  1.017 T  1.017 TT  1.017 M:  13043.0
B  1.017 ..  1.525 T  1.525 TT  1.525 M:  13043.0
B  1.525 ..  2.034 T  2.034 TT  2.034 M:  13043.0
B  2.034 ..  2.542 T  2.542 TT  2.542 M:  13043.0
B  2.542 ..  3.051 T  3.051 TT  3.051 M:  13043.0
B  3.051 ..  3.559 T  3.559 TT  3.559 M:  13043.0
B  3.559 ..  4.068 T  4.068 TT  4.068 M:  13043.0
B  4.068 ..  4.576 T  4.576 TT  4.576 M:  13043.0
B  4.576 ..  5.085 T  5.085 TT  5.085 M:  13043.0
B  5.085 ..  5.593 T  5.593 TT  5.593 M:  13043.0
B  5.593 ..  6.102 T  6.102 TT  6.102 M:  13043.0
B  6.102 ..  6.610 T  6.610 TT  6.610 M:  13043.0
B  6.610 ..  7.119 T  7.119 TT  7.119 M:  13043.0
B  7.119 ..  7.627 T  7.627 TT  7.627 M:  13043.0
B  7.627 ..  8.136 T  8.136 TT  8.136 M:  13043.0
B  8.136 ..  8.644 T  8.644 TT  8.644 M:  13043.0
B  8.644 ..  9.152 T  9.153 TT  9.153 M:  13043.0
B  9.152 ..  9.661 T  9.661 TT  9.661 M:  13043.0
B  9.661 .. 10.169 T 10.169 TT 10.169 M:  13043.0
B 10.169 .. 10.678 T 10.678 TT 10.678 M:  13043.0
B 10.678 .. 11.186 T 11.186 TT 11.186 M:  13043.0
B 11.186 .. 11.695 T 11.695 TT 11.695 M:  13043.0
B 11.695 .. 12.203 T 12.203 TT 12.203 M:  13043.0
B 12.203 .. 12.712 T 12.712 TT 12.712 M:  13043.0
B 12.712 .. 13.220 T 13.220 TT 13.220 M:  13043.0
B 13.220 .. 13.729 T 13.729 TT 13.729 M:  13043.0
B 13.729 .. 14.237 T 14.237 TT 14.237 M:  13043.0
B 14.237 .. 14.491 T 14.492 TT 14.492 M:  13043.0
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  13043.0
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.020s, CPU: 0.020s
256 512 sample blks of shorts written to ./ritmo0.wav (WAV)
```

![pulsazione](./ritmo0.png)

### [Accelerandi lineari - `accelerando0.py`](./accelerando0.py)

```python
from nota import Nota

def tempo_fun(t,start_metro,end_metro,dur_metro):
    a=(end_metro-start_metro)/dur_metro
    b=start_metro
    return a*t+b

def generate(num_note,s_metro,e_metro):
  step=60.0/s_metro
  result=[]
  at=0.0
  for n in range(0,num_note-1):
      result.append(Nota(at,step/2,500,-8,1))
      n_step=60.0/tempo_fun(at,s_metro,e_metro,step*num_note)
      at+=n_step
  return result

note=generate(30,118,300)
print("f1 0 4096 10 1")
for n in note:
  print(n.to_csound())
```

```sh
$ python accelerando0.py > score.sco
$ csound -dWo ./accelerando0.wav driver_1.orc score.sco
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
orchname:  driver_1.orc
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.036s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.036s, CPU: 0.003s
displays suppressed
0dBFS level = 32768.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to ./accelerando0.wav (WAV)
SECTION 1:
ftable 1:
new alloc for instr 1:
B  0.000 ..  0.508 T  0.508 TT  0.508 M:  13043.0
B  0.508 ..  0.992 T  0.992 TT  0.992 M:  13043.0
B  0.992 ..  1.454 T  1.454 TT  1.454 M:  13043.0
B  1.454 ..  1.897 T  1.898 TT  1.898 M:  13043.0
B  1.897 ..  2.324 T  2.324 TT  2.324 M:  13043.0
B  2.324 ..  2.736 T  2.736 TT  2.736 M:  13043.0
B  2.736 ..  3.134 T  3.134 TT  3.134 M:  13043.0
B  3.134 ..  3.520 T  3.520 TT  3.520 M:  13043.0
B  3.520 ..  3.895 T  3.895 TT  3.895 M:  13043.0
B  3.895 ..  4.260 T  4.260 TT  4.260 M:  13043.0
B  4.260 ..  4.615 T  4.615 TT  4.615 M:  13043.0
B  4.615 ..  4.962 T  4.962 TT  4.962 M:  13043.0
B  4.962 ..  5.301 T  5.301 TT  5.301 M:  13043.0
B  5.301 ..  5.632 T  5.632 TT  5.632 M:  13043.0
B  5.632 ..  5.956 T  5.956 TT  5.956 M:  13043.0
B  5.956 ..  6.273 T  6.273 TT  6.273 M:  13043.0
B  6.273 ..  6.584 T  6.584 TT  6.584 M:  13043.0
B  6.584 ..  6.889 T  6.890 TT  6.890 M:  13043.0
B  6.889 ..  7.189 T  7.189 TT  7.189 M:  13043.0
B  7.189 ..  7.484 T  7.484 TT  7.484 M:  13043.0
B  7.484 ..  7.773 T  7.773 TT  7.773 M:  13043.0
B  7.773 ..  8.058 T  8.058 TT  8.058 M:  13043.0
B  8.058 ..  8.338 T  8.338 TT  8.338 M:  13043.0
B  8.338 ..  8.614 T  8.614 TT  8.614 M:  13043.0
B  8.614 ..  8.886 T  8.886 TT  8.886 M:  13043.0
B  8.886 ..  9.153 T  9.154 TT  9.154 M:  13043.0
B  9.153 ..  9.418 T  9.418 TT  9.418 M:  13043.0
B  9.418 ..  9.678 T  9.678 TT  9.678 M:  13043.0
B  9.678 ..  9.932 T  9.932 TT  9.932 M:  13043.0
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  13043.0
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.082s, CPU: 0.015s
256 512 sample blks of shorts written to ./accelerando0.wav (WAV)
```

### [Funzione esponenziale condizionata con offset](./expcon.m)

Funzione esponenziale semplice:

![Funzione esponenziale semplice](./exp_function.png)

Funzione esponenziale condizionata con offset:

![Funzione esponenziale condizionata con offset](./exp_conditioned_function.png)

```matlab
function y=expcon(x,y_start,y_end,c,tau,x_len)
  v_start=y_start-c-tau;
  v_end=y_end-c-tau;
  a=(log(v_end)-log(v_start))/x_len;
  b=log(v_start);
  y=exp(a*x+b)+c+tau;
end
```

```sh
$ octave -q
octave:2> y_start=0.5
y_start =  0.50000
octave:3> y_end=0.005
y_end =  0.0050000
octave:4> tau = 0.000001
tau =  0.0000010000
octave:5> x=[0:0.01:10];
octave:6> y = expcon(x, y_start, y_end, y_end, tau, x(end));
octave:7> plot(x, y)
octave:8> quit
```

![expcon](./expcon.png)

### [Accelerando esponenziale](./accelerando1.py)

```python
from nota import Nota
from math import exp,log

def tempo_fun(t,start_metro,end_metro,dur_metro,tau):
    c=(60.0/end_metro)
    v_start=(60.0/start_metro)-c-tau
    v_end=tau
    a=(log(v_end)-log(v_start))/dur_metro
    b=log(v_start)
    ris=exp(a*t+b)+c+tau
    return ris

def generate(dur,s_metro,e_metro,tau):
  step=60.0/s_metro
  result=[]
  at=0.0
  while at<dur:
      result.append(Nota(at,step/2,500,-8,1))
      n_step=tempo_fun(at,s_metro,e_metro,dur,tau)
      at+=n_step
  return result

note=generate(10,118,600,0.01)
print("f1 0 4096 10 1")
for n in note:
  print(n.to_csound())
```

```sh
$ python accelerando1.py > score.sco
$ csound -dWo ./accelerando1.wav driver_1.orc score.sco
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
orchname:  driver_1.orc
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.002s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.002s, CPU: 0.002s
displays suppressed
0dBFS level = 32768.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to ./accelerando1.wav (WAV)
SECTION 1:
ftable 1:
new alloc for instr 1:
B  0.000 ..  0.508 T  0.508 TT  0.508 M:  13043.0
B  0.508 ..  0.949 T  0.949 TT  0.949 M:  13043.0
B  0.949 ..  1.340 T  1.340 TT  1.340 M:  13043.0
B  1.340 ..  1.693 T  1.693 TT  1.693 M:  13043.0
B  1.693 ..  2.017 T  2.017 TT  2.017 M:  13043.0
B  2.017 ..  2.316 T  2.316 TT  2.316 M:  13043.0
B  2.316 ..  2.596 T  2.596 TT  2.596 M:  13043.0
B  2.596 ..  2.859 T  2.859 TT  2.859 M:  13043.0
B  2.859 ..  3.108 T  3.108 TT  3.108 M:  13043.0
new alloc for instr 1:
B  3.108 ..  3.345 T  3.345 TT  3.345 M:  13041.9
B  3.345 ..  3.571 T  3.571 TT  3.571 M:  13040.6
B  3.571 ..  3.788 T  3.788 TT  3.788 M:  13046.5
B  3.788 ..  3.996 T  3.996 TT  3.996 M:  13038.0
B  3.996 ..  4.198 T  4.198 TT  4.198 M:  13040.7
B  4.198 ..  4.393 T  4.393 TT  4.393 M:  13039.1
B  4.393 ..  4.582 T  4.581 TT  4.581 M:  13034.4
B  4.582 ..  4.765 T  4.765 TT  4.765 M:  13030.1
B  4.765 ..  4.944 T  4.944 TT  4.944 M:  13058.6
B  4.944 ..  5.118 T  5.118 TT  5.118 M:  13032.3
B  5.118 ..  5.289 T  5.289 TT  5.289 M:  13052.2
B  5.289 ..  5.456 T  5.456 TT  5.456 M:  13041.6
B  5.456 ..  5.619 T  5.619 TT  5.619 M:  13028.0
B  5.619 ..  5.779 T  5.779 TT  5.779 M:  13041.5
B  5.779 ..  5.937 T  5.937 TT  5.937 M:  13080.8
B  5.937 ..  6.091 T  6.091 TT  6.091 M:  13025.0
B  6.091 ..  6.244 T  6.244 TT  6.244 M:  12999.5
B  6.244 ..  6.393 T  6.393 TT  6.393 M:  13089.7
B  6.393 ..  6.541 T  6.541 TT  6.541 M:  13095.9
B  6.541 ..  6.687 T  6.687 TT  6.687 M:  13096.6
B  6.687 ..  6.831 T  6.831 TT  6.831 M:  13096.2
B  6.831 ..  6.973 T  6.973 TT  6.973 M:  13094.4
B  6.973 ..  7.114 T  7.114 TT  7.114 M:  13107.5
B  7.114 ..  7.253 T  7.253 TT  7.253 M:  13022.3
B  7.253 ..  7.390 T  7.390 TT  7.390 M:  12958.6
B  7.390 ..  7.526 T  7.526 TT  7.526 M:  13017.8
B  7.526 ..  7.661 T  7.661 TT  7.661 M:  13103.3
B  7.661 ..  7.795 T  7.795 TT  7.795 M:  12988.1
B  7.795 ..  7.927 T  7.927 TT  7.927 M:  13121.6
B  7.927 ..  8.059 T  8.059 TT  8.059 M:  13069.5
B  8.059 ..  8.189 T  8.189 TT  8.189 M:  13047.5
B  8.189 ..  8.319 T  8.319 TT  8.319 M:  13085.6
B  8.319 ..  8.447 T  8.447 TT  8.447 M:  13032.7
B  8.447 ..  8.575 T  8.575 TT  8.575 M:  12945.9
B  8.575 ..  8.702 T  8.702 TT  8.702 M:  13103.8
B  8.702 ..  8.828 T  8.828 TT  8.828 M:  12949.6
new alloc for instr 1:
B  8.828 ..  8.954 T  8.954 TT  8.954 M:  13174.4
B  8.954 ..  9.078 T  9.078 TT  9.078 M:  13092.5
B  9.078 ..  9.202 T  9.202 TT  9.202 M:  12956.8
B  9.202 ..  9.326 T  9.326 TT  9.326 M:  13186.3
B  9.326 ..  9.448 T  9.448 TT  9.448 M:  12974.9
B  9.448 ..  9.571 T  9.571 TT  9.571 M:  12967.1
B  9.571 ..  9.692 T  9.692 TT  9.692 M:  13068.7
B  9.692 ..  9.814 T  9.814 TT  9.814 M:  13062.8
B  9.814 ..  9.934 T  9.934 TT  9.934 M:  12950.8
B  9.934 ..  9.947 T  9.947 TT  9.947 M:  12980.3
B  9.947 .. 10.068 T 10.068 TT 10.068 M:   9037.8
B 10.068 .. 10.189 T 10.188 TT 10.188 M:    110.5
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  13186.3
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.016s, CPU: 0.016s
256 512 sample blks of shorts written to ./accelerando1.wav (WAV)
```

![accelerando 1](./accelerando1.png)

## Realizzazioni dei singoli studenti

### [Francesco Ferracuti - test 2](./Francesco/2/README.md)

### [Francesco Ferracuti - test 3](./Francesco/3/README.md)

## Compiti

* realizzare un breve frammento (max. 60 sec) con sviluppo temporale
  segmentato
