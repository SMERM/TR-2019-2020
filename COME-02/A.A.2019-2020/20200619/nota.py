class Nota:
    def __init__(self,at,dur,freq,amp,instr):
        self.at=at
        self.dur=dur
        self.freq=freq
        self.amp=amp
        self.instr=instr
        
    def to_csound(self):
        a= "i%d %8.4f %8.4f %8.4f %8.4f" % (self.instr,self.at,self.dur,self.freq,self.amp)
        return a 
        
