sr=44100
ksmps=10
nchnls=1

instr 1
iamp=ampdbfs(p5)
ifreq=p4
idur=p3
itable=p6

kenv expseg iamp*0.001,idur*0.01,iamp,idur*0.999,iamp*0.0001
aout oscil kenv,ifreq,itable
     out aout
endin
