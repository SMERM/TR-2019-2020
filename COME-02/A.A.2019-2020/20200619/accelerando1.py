from nota import Nota
from math import exp,log

def tempo_fun(t,start_metro,end_metro,dur_metro,tau):
    c=(60.0/end_metro)
    v_start=(60.0/start_metro)-c-tau
    v_end=tau
    print(";v_start=%16.12f,v_end=%16.12f" %(v_start,v_end))
    a=(log(v_end)-log(v_start))/dur_metro
    b=log(v_start)
    ris=exp(a*t+b)+c+tau
    print(";a=%16.12f,b=%16.12f,ris=%16.12f" %(a,b,ris))
    return ris
def generate(dur,s_metro,e_metro,tau):
  step=60.0/s_metro
  result=[]
  at=0.0
  while at<dur:
      result.append(Nota(at,step/2,500,-8,1))
      n_step=tempo_fun(at,s_metro,e_metro,dur,tau)
      at+=n_step
  return result

note=generate(10,118,600,0.01)
print("f1 0 4096 10 1")
for n in note:
  print(n.to_csound())
