# Lezione di lunedì 24 febbraio 2020

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Lavagna

![whiteboard 1](./TR_I_2020-02-24_19.00.44_1.jpg)

## Argomenti

### Composizione Musicale Elettroacustica (2 ore)

* Strumentario della composizione musicale elettroacustica:
  * tecniche di sintesi
    * additiva
      * onde sintetiche
        * dente di sega (1/n)
        * quadra (1/n per n dispari)
        * triangolare (1/n² per n dispari a fasi alternate)

#### Compiti per casa

* *dovetailing* tra frammenti e suoni sintetici

### Elaborazione Numerica dei Segnali (2 ore)

* Analisi della forma d'onda
  * *Discrete Fourier Transform* (*DFT*)
    * realizzazioni in `octave`:
      * scomposizione di segnali reali (un segmento)

#### Codice `octave`

##### [DFT di un segmento di segnale reale](./dft0.m)

```matlab
[y fc]=audioread("trumpet_wav.wav");
y = y'; 

nsamples=1000


dur=length(y)/fc;
sinc=1/fc;
t=[0:sinc:dur-sinc];
binsize=fc/nsamples;
f=[-fc/2:binsize:fc/2-binsize];
yfrag = y(5000 : 5000+nsamples-1);
tfrag = t(5000 : 5000+nsamples-1);

mydft = zeros(1, nsamples);

for k = 1 : length(f)
  anal = e.^(-i*2*pi*f(k)*tfrag);
  inter= anal .* yfrag;
  temp = sum(inter);
  mydft(1,k) = temp;
end

mag = abs(mydft)/nsamples;


figure(1)
stem(f, mag*2)
axis([0 7000])
```

Questo script produce il seguente grafico:

![dft0 plot](./dft0.jpg)

#### Compiti per casa

* scomposizione di segnali reali armonici e inarmonici
