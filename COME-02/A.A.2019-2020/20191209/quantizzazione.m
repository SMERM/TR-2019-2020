[snd fc] = audioread ("babay.wav");
nbits = [2 3 4 8 12 16];
out = zeros(length(snd)*length(nbits),1);
dur = length(snd)/fc;

for k = 1:length(nbits)
	curbits = 2^nbits(k);
	curstep = 2/curbits ;
	invcurstep = 1/curstep ;
	globstart= (length(snd)*(k-1)) + 1;
	tmpout = round(snd * invcurstep) / invcurstep ;
	out(globstart : globstart + length(tmpout)-1) = tmpout ; 
end
audiowrite("babayriquantizzato.wav",out,fc)
