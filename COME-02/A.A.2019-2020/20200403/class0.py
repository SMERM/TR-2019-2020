class Nota:
    def __init__ (self, stringa):
        (self.data, self.at, self.dur, self.freq, self.amp, self.table, resto)=stringa.split(',',6)
        self.instr = 3
        self.condizionati()

    def condizionati(self):
        self.at   = float(self.at)
        self.dur  = float(self.dur)
        self.amp  = float(self.amp)
        self.freq = float(self.freq)
        self.table= float(self.table)

    def csound(self):
        print("i%d %8.4f %8.4f %+8.2f %8.4f %d" % (self.instr,self.at, self.dur, self.amp, self.freq, self.table))

class Condizionatore:
    def __init__ (self,nomefile,start=1,stop=-1):
        file=open(nomefile,"r")
        righe=file.readlines()
        file.close()

        self.note=[]
        self.start=start
        self.stop=stop
        for line in righe[self.start:self.stop]:
            self.note.append(Nota(line))

        self.condiziona_il_tutto()

    def csound(self):
        for n in self.note[self.start:self.stop]:
            n.csound()

    def condiziona_il_tutto(self):
        self.condiziona_gli_at()
        self.condiziona_i_dur()
        self.condiziona_gli_amp()
        self.condiziona_le_freq()
        self.condiziona_le_table()

    def condiziona_gli_at(self):
        min_at=1e10
        for n in self.note[self.start:self.stop]:
            if n.at < min_at:
                min_at=n.at
        for n in self.note[self.start:self.stop]:
            n.at-=min_at

    def condiziona_i_dur(self,min=0.05,max=2):
        input_range=max-min
        real_min = 1e10
        real_max = 0
        for n in self.note[self.start:self.stop]:
            if n.dur < real_min:
                real_min=n.dur
            if n.dur > real_max:
                real_max=n.dur
        real_range=real_max-real_min
        norm=input_range/real_range
        for n in self.note[self.start:self.stop]:
            n.dur=n.dur*norm+min
        print(real_min,real_max, norm)

    def condiziona_gli_amp(self):
        pass

    def condiziona_le_freq(self):
        pass

    def condiziona_le_table(self):
        pass

c = Condizionatore("tsla.txt")
c.csound()
