# Lezione di venerdì 3 aprile 2020 (lezione svolta in remoto via zoom)

## Ripresa dei lavori

* Correzione dei compiti
* Risposta alle eventuali domande

## Argomenti

### Composizione Musicale Elettroacustica (2 ore)

* Strumentario della composizione musicale elettroacustica:
  * utilizzazione di linguaggi generici (in questo caso: `python`) per la
    realizzazione di partiture `csound`:
    * estensione orientata agli oggetti:
      * introduzione agli *oggetti software*
      * classe `Nota`
      * classe `Condizionatore`

### [codice `python` (`class0.py`)](./class0.py)

```python
class Nota:
    def __init__ (self, stringa):
        (self.data, self.at, self.dur, self.freq, self.amp, self.table, resto)=stringa.split(',',6)
        self.instr = 3
        self.condizionati()

    def condizionati(self):
        self.at   = float(self.at)
        self.dur  = float(self.dur)
        self.amp  = float(self.amp)
        self.freq = float(self.freq)
        self.table= float(self.table)

    def csound(self):
        print("i%d %8.4f %8.4f %+8.2f %8.4f %d" % (self.instr,self.at, self.dur, self.amp, self.freq, self.table))

class Condizionatore:
    def __init__ (self,nomefile,start=1,stop=-1):
        file=open(nomefile,"r")
        righe=file.readlines()
        file.close()

        self.note=[]
        self.start=start
        self.stop=stop
        for line in righe[self.start:self.stop]:
            self.note.append(Nota(line))

        self.condiziona_il_tutto()

    def csound(self):
        for n in self.note[self.start:self.stop]:
            n.csound()

    def condiziona_il_tutto(self):
        self.condiziona_gli_at()
        self.condiziona_i_dur()
        self.condiziona_gli_amp()
        self.condiziona_le_freq()
        self.condiziona_le_table()

    def condiziona_gli_at(self):
        min_at=1e10
        for n in self.note[self.start:self.stop]:
            if n.at < min_at:
                min_at=n.at
        for n in self.note[self.start:self.stop]:
            n.at-=min_at

    def condiziona_i_dur(self,min=0.05,max=2):
        input_range=max-min
        real_min = 1e10
        real_max = 0
        for n in self.note[self.start:self.stop]:
            if n.dur < real_min:
                real_min=n.dur
            if n.dur > real_max:
                real_max=n.dur
        real_range=real_max-real_min
        norm=input_range/real_range
        for n in self.note[self.start:self.stop]:
            n.dur=n.dur*norm+min
        print(real_min,real_max, norm)

    def condiziona_gli_amp(self):
        pass

    def condiziona_le_freq(self):
        pass

    def condiziona_le_table(self):
        pass

c = Condizionatore("tsla.txt",1,10)
c.csound()
```

Con i dati contenuti in [questo file](./tsla.txt), il codice produce ora
[questo risultato](./score.sco).

Questo non è il risultato atteso, considerando the i limiti di durata imposti
per una gamma di durate ![range0](./range0.png), mentre molte durate
chiaramente superano questa gamma.

## Compiti per casa

### Composizione

* correggere la funzione `condiziona_i_dur` in modo da produrre il risultato atteso
* completare il condizionamento dei dati in maniera da produrre un risultato accettabile musicalmente

### Ascolti e discussione

* Ascoltare alcuni *Études* di Pierre Schaeffer, informandosi sul compositore e sulla loro genesi:
  * *Étude aux chemins de fer*
  * *Étude aux casseroles*
  * *Étude aux tourniquets*
  * *Étude de bruits*
* Produrre un canovaccio analitico da discutere in classe
