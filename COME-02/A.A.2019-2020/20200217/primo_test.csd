<CsoundSynthesizer>
<CsOptions>
-o primo_test.wav -W

</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 1
0dbfs = 1

instr 1
a1 soundin "sample_spoon.wav"
out a1
endin

instr 2
a1 diskin "sample_spoon.wav", p4
out a1
endin

instr 3
kfreq oscil 50, 0.8, 2
kfreq = kfreq + 261.6
a1 loscil 1, kfreq, 1, 261.6, 2, 5000, 35000 
out a1
endin



</CsInstruments>
<CsScore>
f1 0 0 1 "sample_spoon.wav" 0 0 0
f2 0 4096 10 1

i1 0    0.1
i1 0.15 0.1
i1 0.35 0.1

i2 1    1   1
i2 2    2   0.25

i3 4	   8  
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
