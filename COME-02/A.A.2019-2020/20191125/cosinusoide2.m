fc = 400; %freq. di campionamento
sinc = 1/fc; %periodo di camp.
dur = 1;  %durata in secondi
freq = 401;  %frequenza in Hz
t = [0:sinc:dur-sinc]; %asse del tempo
y = cos (2*pi*t*freq); 
stem (t,y)
%axis ([0.05 0.06 -2 2]) 
%fenomeno del fold over

