fc1 = 48000; %freq. di campionamento alta
fc2 = 400; %freq di camp bassa
sinc1 = 1/fc1; %periodo di camp.1
sinc2 = 1/fc2; %periodo 2
dur = 1;  %durata in secondi
freq = 802;  %frequenza in Hz
t1 = [0:sinc1:dur-sinc1]; %asse del tempo1
t2 = [0:sinc2:dur-sinc2]; 
y1 = cos (2*pi*t1*freq); 
y2 = cos (2*pi*t2*freq);
hold on
plot (t1,y1)
axis ([0.05 0.07])
stem (t2,y2) 
hold off
%fenomeno del fold over

