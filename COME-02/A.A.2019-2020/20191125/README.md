# Lezione di lunedì 25 novembre 2019

## Argomenti

* campionamento:
  * frequenza di campionamento
  * frequenza di Nyquist
  * *aliasing*
  * *foldover*

## Lavagne

![lavagna 1](./TR_I_2019-11-25_18.39.33.jpg)

## Realizzazione

### [Creare una cosinusoide a 200 Hz (frequenza di campionamento: 48000)](./cosinusoide.m)

```matlab
fc = 48000; %freq. di campionamento
sinc = 1/fc; %periodo di camp.
dur = 0.1;  %durata in secondi
freq = 200;  %frequenza in Hz
t = [0:sinc:dur-sinc]; %asse del tempo
y = cos (2*pi*t*freq); 
plot (t,y)
axis ([0.05 0.06 -2 2])
```

Questo script produce il seguente plot:

![cosinusoide a 200 Hz/fc = 48000 Hz](./cosinusoide.jpg)

### [Verifica del fenomeno del *foldover* con una cosinusoide a 401 Hz con frequenza di campionamento a 400 Hz](./cosinusoide2.m)

```matlab
fc = 400; %freq. di campionamento
sinc = 1/fc; %periodo di camp.
dur = 1;  %durata in secondi
freq = 401;  %frequenza in Hz
t = [0:sinc:dur-sinc]; %asse del tempo
y = cos (2*pi*t*freq); 
stem (t,y)
%axis ([0.05 0.06 -2 2]) 
%fenomeno del fold over
```

L'onda risultante è a 1 Hz per via del fenomeno del *foldover*, QED.

![cosinusoide a 401 Hz/fc = 400 Hz](./cosinusoide2.jpg)

### [Verifica della sovrapposizione di punti di campionamento di una cosinusoide di frequenza 802 Hz campionata sia a 48000 Hz che a 400 Hz](./cosinusoide3.m)

```matlab
fc1 = 48000; %freq. di campionamento alta
fc2 = 400; %freq di camp bassa
sinc1 = 1/fc1; %periodo di camp.1
sinc2 = 1/fc2; %periodo 2
dur = 1;  %durata in secondi
freq = 802;  %frequenza in Hz
t1 = [0:sinc1:dur-sinc1]; %asse del tempo1
t2 = [0:sinc2:dur-sinc2]; 
y1 = cos (2*pi*t1*freq); 
y2 = cos (2*pi*t2*freq);
hold on
plot (t1,y1)
axis ([0.05 0.07])
stem (t2,y2) 
hold off
%fenomeno del fold over
```

La cosinusoide sotto-campionata descriverà un ciclo di 2 Hz, ma i campioni
coincideranno esattamente con l'onda campionata correttamente e di ciclo 802
Hz.

![cosinusoide a 802 Hz/fc = 48 kHz *e* 400 Hz](./cosinusoide3.jpg)

### [brano campionato alle frequenze di campionamento: 1 10 100 1000 10000 22050 44100](./campionamento.m)

```matlab
[snd fc] = audioread ("babay.wav");
fcs = [1 10 100 1000 10000 22050 44100];
out = zeros(length(snd)*length(fcs),1);
dur = length(snd)/fc;
for k = 1:length(fcs)
	curfc = fcs(k);
	cursinc = round(fc/curfc);
	globstart= length(snd)*(k-1);
	m=0;
	for n = 1:cursinc:length(snd)
		tmpout = ones(cursinc,1)*snd(n);
		startsnd = globstart+m*cursinc+1;
		out(startsnd:startsnd+length(tmpout)-1,1)=tmpout;
		m+=1;
	end
end
audiowrite("babayricampionatobuono.wav",out,fc)
```

![campionamento](./campionamento.jpg)
